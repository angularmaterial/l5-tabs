import { NgModule } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule, MatTabsModule } from '@angular/material';

@NgModule({
  imports: [
    NoopAnimationsModule,
    MatIconModule,
    MatTabsModule
  ],
  exports: [
    NoopAnimationsModule,
    MatIconModule,
    MatTabsModule
  ],
})
export class MaterialComponentsModule { }
